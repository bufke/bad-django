from django.contrib.auth.models import User
from django.db import connection
from django.views.generic.list import ListView


class HomeListView(ListView):
    model = User

    def get_queryset(self):
        users = User.objects.none()
        username = self.request.GET.get("username")
        if username:
            with connection.cursor() as cursor:
                # What could go wrong
                cursor.execute(f"SELECT id FROM auth_user WHERE username='{username}'")
                row = cursor.fetchone()
            if row:
                users = super().get_queryset()
        return users
